from pyramid.view import view_config
from pyramid.httpexceptions import HTTPNotFound, HTTPFound

from ..forms import BlogCreateForm, BlogUpdateForm
from ..models.blog import Blog
from ..services.blog import BlogService


@view_config(route_name='blog_show',
             renderer='pblog:templates/blog/show_blog.jinja2')
def blog_show(request):
    blog_id = int(request.matchdict.get('id', -1))
    entry = BlogService.by_id(blog_id, request)
    if not entry:
        return HTTPNotFound()
    return {'entry': entry}


@view_config(route_name='blog_action',
             match_param='action=create',
             renderer='pblog:templates/blog/edit_blog.jinja2',
             permission='create')
def blog_create(request):
    entry = Blog()
    form = BlogCreateForm(request.POST)
    if request.method == 'POST' and form.validate():
        form.populate_obj(entry)
        request.dbsession.add(entry)
        return HTTPFound(location=request.route_url('home'))
    return {'form': form, 'action': request.matchdict['action']}


@view_config(route_name='blog_action',
             match_param='action=edit',
             renderer='pblog:templates/blog/edit_blog.jinja2',
             permission='edit')
def blog_update(request):
    # Second param will be return if not exist id in URL string.
    blog_id = int(request.params.get('id', -1))
    entry = BlogService.by_id(blog_id, request)
    if not entry:
        return HTTPNotFound()
    form = BlogUpdateForm(request.POST, entry)
    if request.method == 'POST' and form.validate():
        form.populate_obj(entry)
        return HTTPFound(
            location=request.route_url('blog_show', id=entry.id, slug=entry.slug))
    return {'form': form, 'action': request.matchdict.get('action')}


@view_config(route_name='blog_action',
             match_param="action=delete",
             permission='delete')
def blog_delete(request):
    blog_id = int(request.params.get('id', -1))
    entry = BlogService.by_id(blog_id, request)
    if not entry:
        return HTTPNotFound()
    request.dbsession.delete(entry)
    return HTTPFound(location=request.route_url('home'))
